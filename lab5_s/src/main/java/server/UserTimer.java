package server;

import json.UserInfo;

public class UserTimer {
    private UserInfo userInfo;
    private long time;

    public UserTimer(UserInfo userInfo){
        this.userInfo = userInfo;
        time = System.currentTimeMillis() / 1000;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public long getTime() {
        return time;
    }

    public UserInfo getUserInfo(){
        return userInfo;
    }
}
