package server;

import json.Message;
import json.UserInfo;

import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

public class ServerData extends TimerTask {
    private List<Message> messages;
    private List<UserInfo> users;
    private ArrayList<UserTimer> userTimers;
    private int idMessage;
    private int idUser;

    public ServerData(){
        messages = new ArrayList<>();
        users = new ArrayList<>();
        userTimers = new ArrayList<>();
        idMessage = 0;
        idUser = 0;
    }

    public void addMessage(Message message, int token){
        message.setId(idMessage);
        idMessage++;
        if(token ==  0){
            message.setAuthor("Server");
        } else {
            for (UserInfo userInfo : users) {
                if (userInfo.getToken() == token) {
                    message.setAuthor(userInfo.getUserName());
                }
            }
        }
        messages.add(message);
    }

    public List<Message> getMessages(){
        return messages;
    }

    public UserInfo getUserInfo(int token){
        for(UserInfo userInfo: users){
            if(userInfo.getToken() == token)
                return userInfo;
        }
        return null;
    }

    public List<UserInfo> getUsers(){
        return users;
    }

    public UserInfo addUser(String username){
        UserInfo userInfo = new UserInfo(idUser, username, true);
        userInfo.setToken(username.hashCode());
        idUser++;
        users.add(userInfo);
        userTimers.add(new UserTimer(userInfo));
        return userInfo;
    }

    public boolean checkUser(String userName){
        for(UserInfo userInfo: users){
            if(userInfo.getUserName().equals(userName)){
                return true;
            }
        }
        return false;
    }

    public void deleteUser(int token){
        for(UserInfo userInfo: users){
            if (userInfo.getToken() == token){
                users.remove(userInfo);
                break;
            }
        }
        for(UserTimer userTimer: userTimers){
            if(userTimer.getUserInfo().getToken() == token){
                userTimers.remove(userTimer);
                return;
            }
        }
    }

    public void updateTime(int token){
        for (UserTimer userTimer: userTimers){
            if(userTimer.getUserInfo().getToken() == token){
                userTimer.setTime(System.currentTimeMillis() / 1000);
            }
        }
    }

    @Override
    public void run() {
        long time = System.currentTimeMillis() / 1000;
        for(UserTimer userTimer: userTimers){
            if(time - userTimer.getTime() > 60){
                for(UserInfo userInfo: users){
                    if(userInfo.getToken() == userTimer.getUserInfo().getToken()){
                        users.remove(userInfo);
                        addMessage(new Message("User " + userInfo.getUserName() + " disabled by timeout"), 0);
                        break;
                    }
                }
            }
        }
    }

}
