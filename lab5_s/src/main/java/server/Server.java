package server;

import com.google.gson.Gson;
import handlers.ErrorHandler;
import handlers.GetUserListHandler;
import handlers.LoginHandler;
import handlers.LogoutHandler;
import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.handlers.PathHandler;
import io.undertow.websockets.core.AbstractReceiveListener;
import io.undertow.websockets.core.BufferedTextMessage;
import io.undertow.websockets.core.WebSocketChannel;
import io.undertow.websockets.core.WebSockets;
import json.Message;
import json.MessageList;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

class Server {
    private Undertow server;

    Server() {
        ServerData serverData = new ServerData();
        Timer timer = new Timer(true);
        timer.schedule(serverData, 3000, 3000);


        HttpHandler pathHandler = new PathHandler(Handlers.path()
                .addExactPath("/login", new LoginHandler(serverData))
                .addExactPath("/logout", new LogoutHandler(serverData))
                .addPrefixPath("/users", new GetUserListHandler(serverData))
                .addPrefixPath("/messages", Handlers.websocket((exchange, webSocketChannel) -> {
                    exchange.setResponseHeader("Content-Type", "application/json");
                    String s = exchange.getRequestHeader("Authorization");                           //?????????????????????
                    s = s.replaceAll("\\D+","");
                    int token = Integer.valueOf(s);
                    serverData.updateTime(token);
                    webSocketChannel.getReceiveSetter().set(new AbstractReceiveListener() {
                        @Override
                        protected void onFullTextMessage(WebSocketChannel channel, BufferedTextMessage message) {
                            Gson json = new Gson();
                            String data = message.getData();
                            if(data.equals("Hi")){
                                List<Message> list = serverData.getMessages();
                                MessageList messageList = new MessageList();
                                messageList.setMessages(list);
                                String g = json.toJson(messageList);
                                for (WebSocketChannel ws : channel.getPeerConnections()) {
                                    WebSockets.sendText(g, ws, null);
                                }
                            } else {
                                Message msg = json.fromJson(data, Message.class);
                                serverData.addMessage(msg, token);
                                List<Message> list = new ArrayList<>();
                                list.add(msg);
                                MessageList messageList = new MessageList();
                                messageList.setMessages(list);
                                String g = json.toJson(messageList);
                                for (WebSocketChannel ws : channel.getPeerConnections()) {
                                    WebSockets.sendText(g, ws, null);
                                }
                            }
                        }
                    });
                    webSocketChannel.resumeReceives();
                }))
                .addPrefixPath("/", new ErrorHandler()));
        server = Undertow.builder()
                .addHttpListener(8080, "localhost", pathHandler)
                .build();
    }

    void start() {
        server.start();
    }
}
