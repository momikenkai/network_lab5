package handlers;

import com.google.gson.Gson;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;
import json.Message;
import json.UserInfo;
import server.ServerData;

public class LogoutHandler implements HttpHandler {
    private ServerData serverData;
    private Gson gson;
    public LogoutHandler(ServerData serverData){
        this.serverData = serverData;
        gson = new Gson();
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        exchange.getRequestReceiver().receiveFullBytes((ex, data) ->{
            String s = exchange.getRequestHeaders().getFirst("Authorization");
            s = s.replaceAll("\\D+","");
            int token = Integer.valueOf(s);
            UserInfo userInfo = serverData.getUserInfo(token);
            if (userInfo == null){
                exchange.setStatusCode(403);
                exchange.getResponseSender().send("");
            } else {
                serverData.addMessage(new Message(userInfo.getUserName() + "left"), 0);
                serverData.deleteUser(token);
                exchange.setStatusCode(200);
                exchange.getResponseHeaders().add(new HttpString("Content-Type"), "application/json");
                exchange.getResponseSender().send("");
            }
        });
    }
}
