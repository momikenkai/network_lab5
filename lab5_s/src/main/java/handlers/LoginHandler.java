package handlers;

import com.google.gson.Gson;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;
import json.Login;
import json.UserInfo;
import server.ServerData;

public class LoginHandler implements HttpHandler {
    private ServerData serverData;
    private Gson gson;
    public LoginHandler(ServerData serverData){
        this.serverData = serverData;
        gson = new Gson();
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        exchange.getRequestReceiver().receiveFullBytes((ex, data) -> {
            Login login = gson.fromJson(new String(data), Login.class);   //проверка
            if(serverData.checkUser(login.getUserName())){
                exchange.getResponseHeaders().add(new HttpString("WWW-Authenticate"), "Token realm='Username is already in use'");
                exchange.setStatusCode(401);
                exchange.getResponseSender().send("");
            } else {
                UserInfo userInfo = serverData.addUser(login.getUserName());
                exchange.getResponseHeaders().add(new HttpString("Content-Type"), "application/json");
                exchange.setStatusCode(200);
                exchange.getResponseSender().send(gson.toJson(userInfo));
            }

        });
    }
}
