package handlers;

import com.google.gson.Gson;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;
import json.UserInfo;
import json.UsersList;
import server.ServerData;

import java.util.List;

public class GetUserListHandler implements HttpHandler {
    private ServerData serverData;
    private Gson gson;
    public GetUserListHandler(ServerData serverData){
        this.serverData = serverData;
        gson = new Gson();
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        exchange.getRequestReceiver().receiveFullBytes((ex, data) -> {
            String s = exchange.getRequestHeaders().getFirst("Authorization");
            s = s.replaceAll("\\D+","");
            int token = Integer.valueOf(s);
            serverData.updateTime(token);
            List<UserInfo> list = serverData.getUsers();
            UsersList usersList = new UsersList(list);
            String g = gson.toJson(usersList);
            exchange.setStatusCode(200);
            exchange.getResponseHeaders().add(new HttpString("Content-Type"), "application/json");
            exchange.getResponseSender().send(g);
        });
    }
}
