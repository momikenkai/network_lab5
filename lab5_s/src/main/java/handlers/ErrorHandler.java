package handlers;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

public class ErrorHandler implements HttpHandler {

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        exchange.getRequestReceiver().receiveFullBytes((ex, data) ->{
            exchange.setStatusCode(405);
            exchange.getResponseSender().send("");
        });
    }
}
