package json;

public class Message {
    private String message;
    private int id;
    private String author;

    public Message(String message){
        this.message = message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getMessage() {
        return message;
    }

    public int getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }
}
