package client;

import com.google.gson.Gson;
import json.Message;
import json.MessageList;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okio.ByteString;

import java.util.ArrayList;
import java.util.List;

public class WebSocketListener_ extends okhttp3.WebSocketListener {
    private Gson json = new Gson();
    private ArrayList<Message> messageList = new ArrayList<>();
    private WebSocket ws;
    private String name;

    WebSocketListener_(int token, OkHttpClient client, String name) {
        this.name = name;
        Request messageReq = new Request.Builder()
                .method("GET", null)
                .addHeader("Authorization", "Token " + token)
                .url("http://localhost:8080" + "/messages")
                .build();
        this.ws = client.newWebSocket(messageReq, this);
    }

    void sendMessage(String message) {
        Message messageSend = new Message(message);
        String jsonString = json.toJson(messageSend);
        ws.send(jsonString);
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
        webSocket.send("Hi");
    }

    @Override
    public void onMessage(WebSocket webSocket, String text) {
        MessageList receivedList = json.fromJson(text, MessageList.class);
        if (receivedList != null) {
            List<Message> newMsg = receivedList.getMessages();
            for (Message message : newMsg) {
                if (!message.getAuthor().equals(name) && !messageList.contains(message)) {
                    System.out.println(message.getAuthor() + ": " + message.getMessage());
                    messageList.add(message);
                }
            }
        }
    }

    public void closeWebSocket(){
        this.ws.close(1000, null);
    }

    @Override
    public void onMessage(WebSocket webSocket, ByteString bytes) {
        System.out.println("MESSAGE: " + bytes.hex());
    }

    @Override
    public void onClosing(WebSocket webSocket, int code, String reason) {
        webSocket.close(1000, null);
    }

    @Override
    public void onFailure(WebSocket webSocket, Throwable t, Response response) {
        t.printStackTrace();
    }
}
