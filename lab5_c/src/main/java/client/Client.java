package client;

import com.google.gson.Gson;
import json.Login;
import json.UserInfo;
import json.UsersList;
import okhttp3.*;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class Client {
    private OkHttpClient client;
    private int token;
    private Scanner scanner;
    private Gson gson;
    private boolean login;
    private String name;
    private WebSocketListener webSocketListener;
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    Client(){
        client = new OkHttpClient();
        scanner = new Scanner(System.in);
        gson = new Gson();
        login = false;
    }
    void start() throws IOException {
        login();
        webSocketListener = new WebSocketListener_(token, client, name);
        String message = scanner.nextLine();
        while(!message.equals("/logout")){
            if(!login){
                login();
            }
            if(message.equals("/list")){
                getUsers();
            } else {
                ((WebSocketListener_)webSocketListener).sendMessage(message);

            }
            message = scanner.nextLine();
        }
        ((WebSocketListener_)webSocketListener).closeWebSocket();
        logout();
        
    }

    private void login() throws IOException {
        Response response;
        UserInfo userInfo;
        while (true) {      //нужно ли проверить хэдер?
            System.out.println("Input your username\n");
            String username = scanner.nextLine();
            Login login = new Login(username);
            String json = gson.toJson(login);
            RequestBody body = RequestBody.create(JSON, json);
            Request request = new Request.Builder()
                    .method("POST", body)
                    .addHeader("Content-Type", "application/json")
                    .url("http://localhost:8080/login")
                    .post(body)
                    .build();
            response = client.newCall(request).execute();
            if (response.code() == 200){
                userInfo = gson.fromJson(response.body().string(), UserInfo.class);
                this.login = true;
                break;
            }
        }
        name = userInfo.getUserName();
        token = userInfo.getToken();
    }

    private void logout() throws IOException {
        RequestBody body = RequestBody.create(JSON, "");
        Request request = new Request.Builder()
                .method("POST", body)
                .addHeader("Authorization", "Token " + token)
                .url("http://localhost:8080/logout")
                .build();
        Response response = client.newCall(request).execute();
        if(response.code() == 200)
            return;
    }

    private void getUsers() throws IOException {
        Request requestUsers = new Request.Builder()
                .method("GET", null)
                .addHeader("Authorization", "Token " + token)
                .url("http://localhost:8080/users")
                .build();
        Response response = client.newCall(requestUsers).execute();
        if (response.code() == 200) {                                                             //ВСЕ ПРОВЕРКИ НА КОДЫ!!!
            UsersList usersList = gson.fromJson(response.body().string(), UsersList.class);
            printUsersList(usersList.getUsers());
        } else if(response.code() == 403){
            setLogin(false);
        }
    }

    private void printUsersList(List<UserInfo> users){
        if(!login){
            System.out.println("Connection lost\n");
            return;
        }
        for(UserInfo userInfo: users){
            System.out.println(userInfo.getUserName() + '\n');
        }
    }

    public boolean isLogin(){
        return login;
    }

    public void setLogin(boolean login) {
        this.login = login;
    }
}
